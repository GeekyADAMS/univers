var FSdrop = document.getElementById('FS-drop');
var dropUL1 = document.getElementById('dropUL1');
var AgentDrop = document.getElementById('agent-uni-menu');
var dropUL2 = document.getElementById('dropUL2');
var moreBtn = document.getElementById('more');
var FAQ1 = document.getElementById('FAQ1');
var FAQ2 = document.getElementById('FAQ2');
var FAQ3 = document.getElementById('FAQ3');
var FAQtexts1 = document.getElementById('FAQtexts1');
var FAQtexts2 = document.getElementById('FAQtexts2');
var FAQtexts3 = document.getElementById('FAQtexts3');
var learnMore = document.getElementById('tChild');
var learnMore2 = document.getElementById('tChild2');
var learnMore3 = document.getElementById('tChild3');
var navbar = document.getElementById('navbar');


var scrollPos = 0;

window.addEventListener('scroll', function(){

    if ((document.body.getBoundingClientRect()).top > scrollPos)
        navbar.style.display = 'flex';
    else
        navbar.style.display = 'none';

    scrollPos = document.body.getBoundingClientRect().top
});

FSdrop.onmouseover = () => {
    dropUL1.style.display = 'block';
}
dropUL1.onmouseleave = () => {
    dropUL1.style.display = 'none';
}
AgentDrop.onmouseover = () => {
    dropUL2.style.display = 'block';
}
dropUL2.onmouseleave = () => {
    dropUL2.style.display = 'none';
}

FAQ1.onclick = () => {
    FAQtexts1.style.display = 'flex';
    FAQtexts2.style.display = 'none';
    FAQtexts2.style.display = 'none';

    FAQ2.style.color = 'rgb(153, 152, 158)';
    FAQ2.style.borderBottomColor = 'rgb(153, 152, 158)';
    FAQ3.style.color = 'rgb(153, 152, 158)';
    FAQ3.style.borderBottomColor = 'rgb(153, 152, 158)';

    FAQ1.style.color = 'rgb(80, 211, 211)';
    FAQ1.style.borderBottomColor = 'rgb(80, 211, 211)';
}
FAQ2.onclick = () => {
    FAQtexts1.style.display = 'none';
    FAQtexts2.style.display = 'flex';
    FAQtexts3.style.display = 'none';

    FAQ1.style.color = 'rgb(153, 152, 158)';
    FAQ1.style.borderBottomColor = 'rgb(153, 152, 158)';
    FAQ3.style.color = 'rgb(153, 152, 158)';
    FAQ3.style.borderBottomColor = 'rgb(153, 152, 158)';

    FAQ2.style.color = 'rgb(80, 211, 211)';
    FAQ2.style.borderBottomColor = 'rgb(80, 211, 211)';
}
FAQ3.onclick = () => {
    FAQtexts1.style.display = 'none';
    FAQtexts2.style.display = 'none';
    FAQtexts3.style.display = 'flex';

    FAQ1.style.color = 'rgb(153, 152, 158)';
    FAQ1.style.borderBottomColor = 'rgb(153, 152, 158)';
    FAQ2.style.color = 'rgb(153, 152, 158)';
    FAQ2.style.borderBottomColor = 'rgb(153, 152, 158)';

    FAQ3.style.color = 'rgb(80, 211, 211)';
    FAQ3.style.borderBottomColor = 'rgb(80, 211, 211)';
}

moreBtn.onclick = () => {
    window.scroll({
        top: 700,
        left: 0,
        duration: 4000,
        behavior: 'smooth'
    });
}

function faq(){
    window.scroll({
        top: 3000,
        left: 0,
        duration: 4000,
        behavior: 'smooth'
    });
}

learnMore.onclick = () => {
    faq();
}
learnMore2.onclick = () => {
    faq();
}
learnMore3.onclick = () => {
    faq();
}
